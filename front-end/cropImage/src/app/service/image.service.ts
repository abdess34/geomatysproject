import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ImageService {
  private apiUrl = 'http://localhost:8080/api';
  constructor(private http: HttpClient) {}

  postImageWithRectangle(image: File, text: string): Observable<any> {
    const formData: FormData = new FormData();
    formData.append('file', image);
    formData.append('rec', text);
    console.log('avant envoie Post');
    // Assurez-vous que l'URL est celle de votre API qui accepte ce type de requête
    return this.http.post(this.apiUrl + '/uploadCrop', formData, {
      responseType: 'text',
    });
  }

  getImageCroped(): Observable<any> {
    return this.http.get(this.apiUrl + '/image/cropped.png', {
      responseType: 'blob',
    });
  }
}
