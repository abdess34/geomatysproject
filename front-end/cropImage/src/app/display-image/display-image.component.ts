import { Component } from '@angular/core';
import { ImageCroppedEvent } from 'ngx-image-cropper';
import { ImageService } from '../service/image.service';
import { switchMap } from 'rxjs/operators';
@Component({
  selector: 'app-display-image',
  templateUrl: './display-image.component.html',
  styleUrl: './display-image.component.css',
})
export class DisplayImageComponent {
  imageChangedEvent: any = '';
  croppedImage: any = '';
  originalImageFile: File | null = null;
  ImageProcessed: string | null = null;
  selectedFile: boolean = false;
  selection: {
    x: number;
    y: number;
    width: number;
    height: number;
  };

  constructor(private imageService: ImageService) {
    this.selection = {
      x: 0,
      y: 0,
      width: 0,
      height: 0,
    };
  }

  fileChangeEvent(event: any): void {
    this.imageChangedEvent = event;
    if (event.target.files && event.target.files.length) {
      this.originalImageFile = event.target.files[0]; // Stocker le fichier d'origine
    }
    this.selectedFile = true;
  }
  imageCropped(event: ImageCroppedEvent) {
    this.croppedImage = event.base64;
    // Vous pouvez envoyer event.croppedImage ici à votre API backend pour le traitement
    this.selection.x = event.imagePosition.x1;
    this.selection.y = event.imagePosition.y1;
    this.selection.width = event.width;
    this.selection.height = event.height;
    console.log(this.selection);
  }
  imageLoaded() {
    // show cropper
  }
  cropperReady() {
    // cropper ready
  }
  loadImageFailed() {
    // show message
  }
  uploadCrop() {
    if (this.originalImageFile !== null) {
      /*
      this.imageService
        .postImageWithRectangle(
          this.originalImageFile,
          JSON.stringify(this.selection)
        )
        .subscribe({
          next: (response) => console.log(response),
          error: (error) => console.error(error),
        });

      this.imageService.getImageCroped().subscribe(
        (data) => {
          console.log(data);
        },
        (error) => {
          console.error(error);
        }
      );
    } else {
      console.log('Aucun fichier sélectionné.');
    }
    */

      this.imageService
        .postImageWithRectangle(
          this.originalImageFile,
          JSON.stringify(this.selection)
        )
        .subscribe(
          (postResponse) => {
            console.log('POST Response:', postResponse);

            // Exécuter la requête GET après que la requête POST a réussi
            this.imageService.getImageCroped().subscribe(
              (blob) => {
                const objectURL = URL.createObjectURL(blob);
                this.ImageProcessed = objectURL;
              },
              (getError) => {
                console.error('GET Error:', getError);
              }
            );
          },
          (postError) => {
            console.error('POST Error:', postError);
          }
        );
    }
  }
}
