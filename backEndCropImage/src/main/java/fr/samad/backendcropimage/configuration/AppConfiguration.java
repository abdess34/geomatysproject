package fr.samad.backendcropimage.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.servlet.config.annotation.CorsRegistration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class AppConfiguration implements WebMvcConfigurer {
    @Bean
    public ObjectMapper objectMapper() {
        // Vous pouvez configurer l'ObjectMapper selon vos besoins ici
        return new ObjectMapper();
    }

    @Override
    public void addCorsMappings(CorsRegistry corsRegistry){
        corsRegistry.addMapping("/**").allowedOrigins("http://localhost:4200")
                .allowedMethods("GET","POST").allowedHeaders("*")
                .allowCredentials(true).maxAge(3600);
    }
}
