package fr.samad.backendcropimage.service;


import fr.samad.backendcropimage.model.Rectangle;
import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.awt.image.BufferedImage;
import java.io.IOException;

public interface UploadCropService {


    public void save(BufferedImage b) throws IOException;

    public Resource load(String filename);

    BufferedImage crop(MultipartFile file, Rectangle rec) throws IOException;
}
