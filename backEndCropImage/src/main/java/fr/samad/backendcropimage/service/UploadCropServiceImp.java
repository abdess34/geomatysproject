package fr.samad.backendcropimage.service;

import fr.samad.backendcropimage.model.Rectangle;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Service
public class UploadCropServiceImp implements UploadCropService{
    private final Path root = Paths.get("./uploads");

    @Override
    public Resource load(String filename) {
        try {
            Path file = root.resolve(filename);
            Resource resource = new UrlResource(file.toUri());

            if (resource.exists() || resource.isReadable()) {
                return resource;
            } else {
                throw new RuntimeException("Could not read the file!");
            }
        } catch (MalformedURLException e) {
            throw new RuntimeException("Error: " + e.getMessage());
        }
    }


    public void save(BufferedImage buffer) throws IOException {
        if (!isCreatedDir()) {
            CreateDir();
        }
        File outputfile = new File(root+"/cropped.png");
        ImageIO.write(buffer, "png", outputfile);
    }

    @Override
    public BufferedImage  crop(MultipartFile file, Rectangle rec) throws IOException {
        // Get a BufferedImage object from a byte array
        byte[] image = file.getBytes();

        InputStream in = new ByteArrayInputStream(image);
        BufferedImage originalImage = ImageIO.read(in);

        // Get image dimensions
        int height = originalImage.getHeight();
        int width = originalImage.getWidth();

        BufferedImage croppedImage = originalImage.getSubimage(
                (int) rec.getX(),
                (int) rec.getY(),
                (int) rec.getWidth(),
                (int) rec.getHeight()
        );

        return croppedImage;
    }


    /**
     * Creer le repertoire de sauvegarde
     */
    public void CreateDir() {
        try {
            Files.createDirectories(root);
        } catch (IOException e) {
            throw new RuntimeException("Impossible de creer le rep de sauvegarde!");
        }
    }

    public boolean isCreatedDir(){
        return new File(String.valueOf(root)).exists();
    }
}
