package fr.samad.backendcropimage.controller;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.samad.backendcropimage.model.Rectangle;
import fr.samad.backendcropimage.service.UploadCropService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;


import java.awt.image.BufferedImage;
import java.io.File;

@RestController
public class ImageController {
    @Autowired
    private UploadCropService ucService;

    private final ObjectMapper objectMapper;

    private static final String uri ="api";

    public ImageController(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }


    @PostMapping(uri + "/uploadCrop")
    public String uploadCropImage(@RequestParam("file") MultipartFile file,
                                  @RequestParam("rec") String rectangleJson) throws JsonProcessingException {
        Rectangle rectangle = objectMapper.readValue(rectangleJson, Rectangle.class);

        try {
            BufferedImage croppedImage = ucService.crop(file,rectangle);
            ucService.save(croppedImage);
            return "Uploaded the image successfully: " + file.getOriginalFilename();

        } catch (Exception e) {
            return e.getMessage( );
        }

    }


    @GetMapping(uri + "/image/{filename:.+}")
    public ResponseEntity<Resource> getImage(@PathVariable String filename) {
        Resource file = ucService.load(filename);

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"").contentType(MediaType.IMAGE_JPEG).body(file);
    }


}
