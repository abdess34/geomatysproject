package fr.samad.backendcropimage;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BackEndCropImageApplication {

    public static void main(String[] args) {
        SpringApplication.run(BackEndCropImageApplication.class, args);
    }

}
